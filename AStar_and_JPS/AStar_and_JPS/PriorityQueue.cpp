#include "PriorityQueue.h"

PriorityQueue::PriorityQueue(){}

void PriorityQueue::Push(Node* node)
{
	nodes.insert(std::pair<double, Node*>(node->GetTotalCost(), node));
}

Node* PriorityQueue::Pop()
{
	// 最小の総スコアを取り出す際はstd::map内の先頭イテレータを取り出せばOK
	multimap<double, Node*>::iterator itr = nodes.begin();
	Node* temp = itr->second;
	// ただしデータを取り出した後はイテレータをstd::mapから削除すること
	nodes.erase(itr);
	return temp;
}

bool PriorityQueue::Empty()
{
	return nodes.empty();
}

bool PriorityQueue::Has(const Node* node)
{
	for (multimap<double, Node*>::iterator itr = nodes.begin(); itr != nodes.end(); itr++)
	{
		if (Node::Equal(*node, *itr->second))
			return true;
	}
	return false;
}
