#pragma once

#include <vector>
#include "Node.h"
#include "AStarEnvironment.h"


/* @biref JumpPointSearchアルゴリズム
A*が次に訪れるべきノードの候補を提供する
次に訪れるべきノードというのは「障害物のかど」に該当するノードの事である
例えば以下のような探索状況があった場合（Cが現在訪れているノード、#が壁）
=============
| C |   | # |
|   |   |   |
|   |   |   |
=============
JumpPointSearchが返す候補ノードは壁のかどに該当するノード（N）である
=============
| C |   | # |
|   | N |   |
|   |   |   |
=============
*/
class JumpPointSearch
{
public:
	// コンストラクタ
	JumpPointSearch(AStarEnvironment& env) : env(env) {}

	// 現在訪れているノードから次のノードへ移動するための候補ノードを提供する
	vector<Node*> Neighbours(const Node* path)
	{
		Node* current = new Node(path->GetPosX(), path->GetPosY());
		current->SetParent(path->GetParent());
		Node* parent = path->GetParent();
		vector<Node*> fs = Forwards(current, parent);
		vector<Node*> ns;
		for each (Node* forward in fs)
		{
			Node* node = Jump(forward, current);
			if (node != nullptr)
				ns.push_back(node);
		}
		return ns;
	}

private:
	int Sign(int a)
	{
		return a == 0 ? 0 : (a > 0 ? 1 : -1);
	}

	// 引数で渡された配列に対しノードを格納する
	void Push(vector<Node*>& ns, int x, int y)
	{
		if (!env.IsWall(x, y))
			ns.push_back(new Node(x, y));
	}

	// 
	vector<Node*> Forwards(Node* current, Node* parent)
	{
		if (parent == nullptr)
			return env.Neighbours(current);
		int cx = current->GetPosX(), cy = current->GetPosY();
		int px = parent->GetPosX(), py = parent->GetPosY();
		int dx = Sign(cx - px), dy = Sign(cy - py);
		vector<Node*> fs;
		// 右下方向
		if (dx != 0 && dy != 0)
		{
			// 右下または左下のノードを格納
			Push(fs, cx + dx, cy + dy);
			// 下のノードを格納
			Push(fs, cx + 0, cy + dy);
			// 左または右のノードを格納
			Push(fs, cx + dx, cy + 0);

			// 左のノードは壁か？
			if (env.IsWall(cx - dx, cy)) Push(fs, cx - dx, cy + dy);	// 左下のノードを格納
			// 上のノードは壁か？
			if (env.IsWall(cx, cy - dy)) Push(fs, cx + dx, cy - dy);	// 右上のノードを格納
			return fs;
		}
		// 左または右方向
		else if (dy == 0)
		{
			// 左または右のノードを格納
			Push(fs, cx + dx, cy);

			// 下のノードは壁か？
			if (env.IsWall(cx, cy + 1)) Push(fs, cx + dx, cy + 1);	// 右下のノードを格納
			// 上のノードは壁か？
			if (env.IsWall(cx, cy - 1)) Push(fs, cx + dx, cy - 1);	// 右上のノードを格納
			return fs;
		}
		// 上または下方向
		else if (dx == 0)
		{
			// 上または下のノードを取得
			Push(fs, cx, cy + dy);

			// 右のノードは壁か？
			if (env.IsWall(cx + 1, cy)) Push(fs, cx + 1, cy + dy);	// 右下のノードを格納
			// 左のノードは壁か？
			if (env.IsWall(cx - 1, cy)) Push(fs, cx - 1, cy + dy);	// 左下のノードを格納
			return fs;
		}

		return vector<Node*>();
	}

	// 
	Node* Jump(Node* forward, Node* current)
	{
		int fx = forward->GetPosX(), fy = forward->GetPosY();
		if (env.IsWall(fx, fy))
			return nullptr;
		if (Node::Equal(*forward, *env.endNode))
			return forward;
		int cx = current->GetPosX(), cy = current->GetPosY();
		int dx = Sign(fx - cx), dy = Sign(fy - cy);
		// 右下方向へ直進
		if (dx != 0 && dy != 0)
		{
			// 左のノードは壁で、左下のノードは壁ではないか？
			if (env.IsWall(fx - dx, fy) && !env.IsWall(fx - dx, fy + dy))
				return forward;
			// 上のノードは壁で、右上のノードは壁ではないか？
			if (env.IsWall(fx, fy - dy) && !env.IsWall(fx + dx, fy - dy))
				return forward;

			// 下と上のノードに対し再帰処理
			if (Jump(new Node(fx, fy + dy), forward) != nullptr ||
				Jump(new Node(fx + dx, fy), forward) != nullptr)
				return forward;	// 上２つの再帰処理がどちらもnullptrを返さなければ引数の値をそのまま帰す
		}
		// 左または右方向へ直進
		else if (dy == 0)
		{
			// 下のノードは壁で、右下のノードは壁ではないか？
			if (env.IsWall(fx, fy + 1) && !env.IsWall(fx + dx, fy + 1))
				return forward;
			// 上のノードは壁で、右上のノードは壁ではないか？
			if (env.IsWall(fx, fy - 1) && !env.IsWall(fx + dx, fy - 1))
				return forward;
		}
		// 上または下方向へ直進
		else if (dx == 0)
		{
			// 右のノードは壁で、右下のノードは壁ではないか？
			if (env.IsWall(fx + 1, fy) && !env.IsWall(fx + 1, fy + dy))
				return forward;
			// 左のノードは壁で、左上のノードは壁ではないか？
			if (env.IsWall(fx - 1, fy) && !env.IsWall(fx - 1, fy + dy))
				return forward;
		}

		// 直進方向＋１にあるノードに対し再帰処理を行う
		return Jump(new Node(fx + dx, fy + dy), current);
	}

private:
	AStarEnvironment env;
};