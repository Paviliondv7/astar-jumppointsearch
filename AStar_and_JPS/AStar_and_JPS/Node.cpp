#include "Node.h"

Node::Node()
{}

Node::Node(int x, int y) 
	: pos_x(x)
	, pos_y(y)
	, element(NodeElement::NONE)
	, parent(nullptr)
	, totalCost(0.0)
{}

Node::Node(int x, int y, NodeElement element)
	: pos_x(x)
	, pos_y(y)
	, element(element)
	, parent(nullptr)
	, totalCost(0.0)
{}


bool Node::Equal(const Node& lhs, const Node& rhs)
{
	return (lhs.GetPosX() == rhs.GetPosX()) && (lhs.GetPosY() == rhs.GetPosY());
}
