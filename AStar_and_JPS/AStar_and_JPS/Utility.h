#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;


/*! @brief　CSV読み込み関数
@param[in] filename : 読み込むCSVファイル名
@param[in] table : 読み込んだ結果の格納テーブル
@param[in] delimiter : 区切り文字
@return　bool: 読み込み成功したか
*/
static bool ReadCSV(const string& filename, vector<vector<string>>& table, const char delimiter = ',')
{
	fstream filestream(filename);
	if (!filestream.is_open())
		return false;

	while (!filestream.eof())
	{
		string buf;
		filestream >> buf;

		vector<string> record;
		istringstream streambuffer(buf);
		string token;

		while (getline(streambuffer, token, delimiter))
			record.push_back(token);

		table.push_back(record);
	}

	return true;
}