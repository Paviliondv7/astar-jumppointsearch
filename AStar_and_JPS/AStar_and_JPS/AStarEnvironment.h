#pragma once

#include <vector>
#include <list>
#include "Node.h"
#include "Utility.h"

using namespace std;


/* @biref A*やJPSの実行環境
CSVから読み込んだマップデータを２次元配列として保持し
マップデータに関する機能を提供する
*/
class AStarEnvironment
{
public:
	// コンストラクタ
	AStarEnvironment(string filename);

	// 引数で渡された属性と同じ属性を持つノードを返す
	Node* AtNode(NodeElement element);
	// 渡された座標には壁に該当するノードがあるか？
	bool IsWall(int x, int y);
	// スタートノードから引数のノードまでのコストを求める
	double GCost(const Node* node);
	// 引数のノードからゴールノードまでのコストを求める
	double HCost(const Node* node);
	// 引数のノードから縦横斜めの周囲８マス分のノードを取得
	std::vector<Node*> Neighbours(const Node* node);
	// 最終的に得られたノードから親を辿り出力用の経路データを作成する
	std::vector<std::vector<string>> MakeResultPath(Node* node);

private:
	// CSVデータを他のクラスから扱いやすい形へと変換する
	void MakeMapData(string filename);
	// ２つのノードの距離を求める
	double Distance(const Node* a, const Node* b);

public:
	vector<vector<Node*>> mapData;
	Node* startNode;
	Node* endNode;;
};