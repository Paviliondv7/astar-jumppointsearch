#pragma once

#include "Node.h"
#include "PriorityQueue.h"
#include "AStarEnvironment.h"
#include "JumpPointSearch.h"
#include <vector>


/*! @brief A*アルゴリズム
実装内容はWikipediaのA*ページにある擬似コードをそのままC++で書き起こした
[http://ja.wikipedia.org/wiki/A*]
 @param[in] env : マップデータを含む実行環境
 @param[in] jump_point_search : trueにするとJumpPointSearchで経路探索を行う
 @return 
 */
static Node* AStar(AStarEnvironment& env , bool jump_point_search = false)
{ 
	JumpPointSearch jps(env);
	PriorityQueue openList;		// 現在計算中のノードを格納するリスト
	PriorityQueue closeList;	// 計算終了したノードを格納するリスト

	openList.Push(env.startNode);

	while (!openList.Empty())
	{
		Node* n = openList.Pop();
		if (Node::Equal(*n, *env.endNode))
			return n;
		else
			closeList.Push(n);

		vector<Node*> neigbours;
		if (jump_point_search == false)
			neigbours = env.Neighbours(n);
		else
			neigbours = jps.Neighbours(n);
		
		for each (Node* m in neigbours)
		{
			double f = env.GCost(n) + env.HCost(m) + sqrt(pow(n->GetPosX() - m->GetPosX(), 2) + pow(n->GetPosY() - m->GetPosY(), 2));
			if (!openList.Has(m) && !closeList.Has(m))
			{
				m->SetTotalCost(f);
				m->SetParent(n);
				openList.Push(m);
			}
			else if (openList.Has(m))
			{
				if (f < m->GetTotalCost())
				{
					m->SetTotalCost(f);
					m->SetParent(n);
				}
			}
			else if (closeList.Has(m))
			{
				if (f < m->GetTotalCost())
				{
					m->SetTotalCost(f);
					m->SetParent(n);
					openList.Push(m);
				}
			}
		}
	}

	return nullptr;
}
