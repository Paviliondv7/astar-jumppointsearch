#include "AStarEnvironment.h"

AStarEnvironment::AStarEnvironment(string filename)
{
	MakeMapData(filename);
	startNode = AtNode(NodeElement::START);
	endNode = AtNode(NodeElement::GOAL);
}

Node* AStarEnvironment::AtNode(NodeElement element)
{
	for (int y = 0; y < mapData.size(); y++)
		for (int x = 0; x < mapData[y].size(); x++)
			if (mapData[y][x]->GetElement() == element)
				return mapData[y][x];
	return nullptr;
}

bool AStarEnvironment::IsWall(int x, int y)
{
	// 渡された座標がマップデータの範囲外かをチェック
	if (y < 0 || y > mapData.size() - 1 || x < 0 || x > mapData[y].size() - 1)
		return true;
	// 範囲内であれば指定した座標のノードの属性をチェックする
	return mapData[y][x]->GetElement() == NodeElement::WALL;
}

double AStarEnvironment::GCost(const Node* node)
{
	// GCostはノードが持つ総コストからHCostを引く事で求められる
	return (node->GetTotalCost() - HCost(node));
}

double AStarEnvironment::HCost(const Node* node)
{
	// HCostの計算方法はいくつか有り
	// 各座標の差の総和を２点間の距離とする「マンハッタン距離」
	// 通常の２点間の距離である「ユークリッド距離」
	// 等がメジャー
	// HCostを求める関数を「ヒューリスティック関数」と言う
	return Distance(node, endNode);
}


std::vector<Node*> AStarEnvironment::Neighbours(const Node* node)
{
	int px = node->GetPosX(), py = node->GetPosY();
	vector<Node*> neighbor;

	// 左上
	if (!IsWall(px - 1, py - 1))
		neighbor.push_back(mapData[py - 1][px - 1]);
	// 上
	if (!IsWall(px - 0, py - 1))
		neighbor.push_back(mapData[py - 1][px - 0]);
	// 右上
	if (!IsWall(px + 1, py - 1))
		neighbor.push_back(mapData[py - 1][px + 1]);
	// 左
	if (!IsWall(px - 1, py + 0))
		neighbor.push_back(mapData[py - 0][px - 1]);
	// 右
	if (!IsWall(px + 1, py + 0))
		neighbor.push_back(mapData[py - 0][px + 1]);
	// 左下
	if (!IsWall(px - 1, py + 1))
		neighbor.push_back(mapData[py + 1][px - 1]);
	// 下
	if (!IsWall(px + 0, py + 1))
		neighbor.push_back(mapData[py + 1][px - 0]);
	// 右下
	if (!IsWall(px + 1, py + 1))
		neighbor.push_back(mapData[py + 1][px + 1]);

	return neighbor;
}

std::vector<std::vector<string>> AStarEnvironment::MakeResultPath(Node* node)
{
	list<Node*> hoge;
	Node* temp = node;
	while (temp->GetParent() != nullptr)
	{
		hoge.push_front(temp);
		temp = temp->GetParent();
	}

	std::vector<std::vector<string>> buf;
	buf.resize(mapData.size());
	for (int y = 0; y < mapData.size(); y++)
	{
		for (int x = 0; x < mapData[y].size(); x++)
		{
			if (mapData[y][x]->GetElement() == NodeElement::WALL)
				buf[y].push_back("0");
			if (mapData[y][x]->GetElement() == NodeElement::NONE)
				buf[y].push_back(" ");
			if (mapData[y][x]->GetElement() == NodeElement::START)
				buf[y].push_back("S");
			if (mapData[y][x]->GetElement() == NodeElement::GOAL)
				buf[y].push_back("G");
		}
	}

	for each (Node* node in hoge)
	{
		buf[node->GetPosY()][node->GetPosX()] = "*";
	}

	return buf;
}


void AStarEnvironment::MakeMapData(string filename)
{
	std::vector<std::vector<std::string>> temp;
	if (!ReadCSV(filename, temp))
		return;

	mapData.resize(temp.size());
	for (int y = 0; y < temp.size(); y++)
	{
		for (int x = 0; x < temp[y].size(); x++)
		{
			if (temp[y][x] == "")
				mapData[y].push_back(new Node(x, y, NodeElement::NONE));
			else if (temp[y][x] == "0")
				mapData[y].push_back(new Node(x, y, NodeElement::WALL));
			else if (temp[y][x] == "S")
				mapData[y].push_back(new Node(x, y, NodeElement::START));
			else if (temp[y][x] == "G")
				mapData[y].push_back(new Node(x, y, NodeElement::GOAL));
		}
	}
}

double AStarEnvironment::Distance(const Node* a, const Node* b)
{
	// 障害物を考慮しない直線距離を求めている
	return sqrt(pow(a->GetPosX() - b->GetPosX(), 2) + pow(a->GetPosY() - b->GetPosY(), 2));
}
