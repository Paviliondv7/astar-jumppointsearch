#pragma once

enum NodeElement
{
	NONE,
	OPEN,
	CLOSE,
	WALL,
	START,
	GOAL
};

class Node
{
public:
	Node();
	Node(int x, int y);
	Node(int x, int y, NodeElement element);

	static bool Equal(const Node& lhs, const Node& rhs);

	NodeElement GetElement(){ return element; }

	Node* GetParent() const{ return parent; }
	void SetParent(Node* newParent){ parent = newParent; }

	int GetPosX() const{ return pos_x; }
	int GetPosY() const{ return pos_y; }

	double GetTotalCost()const{ return totalCost; }
	void SetTotalCost(double cost){ totalCost = cost; }


private:
	int pos_x, pos_y;
	NodeElement element;
	Node* parent;
	double totalCost;
};