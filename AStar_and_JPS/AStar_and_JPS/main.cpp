#include "AStarEnvironment.h"
#include "AStar.h"
#include <list>
#include <time.h>

AStarEnvironment* env = new AStarEnvironment("MapData.csv");
AStarEnvironment* env2 = new AStarEnvironment("MapData.csv");


void main(void)
{
	clock_t begin, end;

	begin = clock();
	Node* path = AStar(*env);
	end = clock();
	printf("処理時間:%d[ms]\n", end - begin);
	std::vector<std::vector<string>> result = env->MakeResultPath(path);
	for (int i = 0; i < result.size(); i++)
	{
		for (int j = 0; j < result[i].size(); j++)
		{
			std::cout << result[i][j];
		}
		std::cout << std::endl;
	}

	begin = clock();
	path = AStar(*env2, true);
	end = clock();
	printf("処理時間:%d[ms]\n", end - begin);
	result = env2->MakeResultPath(path);
	for (int i = 0; i < result.size(); i++)
	{
		for (int j = 0; j < result[i].size(); j++)
		{
			std::cout << result[i][j];
		}
		std::cout << std::endl;
	}

	std::cout << "Hello World!!" << std::endl;
}