#pragma once

#include "Node.h"
#include <map>

using namespace std;


/* @biref 優先度付きキュー
キューと言っても中身はただのstd::map
std::mapはキーで自動ソートしてくれる
*/
class PriorityQueue
{
public:
	// コンストラクタ
	PriorityQueue();

	// 引数の値を格納
	void Push(Node* node);
	// 最も総スコアの小さいノードを取り出す
	Node* Pop();
	// 要素は空か？
	bool Empty();
	// 引数の値が既に格納されているか？
	bool Has(const Node* node);

private:
	std::multimap<double, Node*> nodes;
};